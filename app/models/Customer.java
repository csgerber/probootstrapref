package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Adam on 6/29/2015.
 */
@Entity
public class Customer extends Model{



    public String name;
    public String email;
    public String phone;
    public String message;

    public Customer() {
    }

    public Customer(String name, String email, String phone, String message) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.message = message;
    }
}
