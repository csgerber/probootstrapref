package controllers;

import models.Customer;
import play.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import static play.data.Form.form;

public class Application extends Controller {


    //requires static import of static play.data.Form.form;
   final static Form<Customer> customerForm = form(Customer.class);

    public Result index() {
        return ok(index.render("Reach", "::Innovation", customerForm));
    }


    public Result submit() {

        //get the data from the form
        Form<Customer> filledForm = customerForm.bindFromRequest();
        if (!filledForm.hasErrors()){
            //Customer customer = new Customer(
            String name =   filledForm.data().get("name");
            String email =   filledForm.data().get("email");
            String phone =   filledForm.data().get("phone");
            String message =   filledForm.data().get("message");
            Customer customer = new Customer(name, email, phone, message);
            //save it to the in mem db
            customer.save();
            //possibly return something else or just refresh the view

        }
        return ok(index.render("Reach", "::Innovation", customerForm));

    }


}
